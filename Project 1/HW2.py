'''Run using python3'''
import sys

def driver():
    graphs = int(sys.stdin.readline().strip())
    for v in range(graphs):
        print("\nGraph Number: {}".format(v+1))
        nodes = int(sys.stdin.readline().strip())
        edges = int(sys.stdin.readline().strip())
        graph = {n : [] for n in range(1, nodes)}
        for edge in range(edges):
            out_node, in_node = sys.stdin.readline().split()
            graph[int(out_node)].append(int(in_node))

        npath = [None]*(nodes+1)
        longest_path = [None]*(nodes+1)
        shortest_path = [None]*(nodes+1)
        for i in range(2, nodes+1):
            npath[i] = 0
            longest_path[i] = 0
            shortest_path[i] = float("Inf")

        npath[1] = 1
        longest_path[1] = 0
        shortest_path[1] = 0
        for i in range(1,nodes):
            for j in graph[i]:
                if j in graph[i]:
                    npath[j] = npath[j]+npath[i]
                    longest_path[j] = max(longest_path[j],longest_path[i]+1)
                    shortest_path[j] = min(shortest_path[j],shortest_path[i]+1)
        print("Shortest path: {}".format(shortest_path[-1]))
        print("Longest path: {}".format(longest_path[-1]))
        print("Number of paths: {}".format(npath[-1]))
        return 0


if __name__ == "__main__":
    driver()