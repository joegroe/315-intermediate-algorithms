import sys



def iterCheck(dict, str, split):
    if (len(str) == 0):
        print("YES, can be split")
        print(split,"\n")
        return True
    else:
        next = ""
        i = 0
        while (i < len(str)):
            next += str[i]
            if next in dict:
                if (iterCheck(dict,str[i+1:], split+next+ " ")):
                    return True
                else:
                    i += 1
            else:
                i += 1
        return False

def memCheck(dict, str, split, cached):
    if len(str) == 0:
        print("YES, can be split")
        print(split,"\n")
        return True
    elif (str in cached):
        return False
    else:
        next = ""
        i = 0;
        while (i < len(str)):
            next += str[i]
            if (next in dict):
                if (memCheck(dict,str[i+1:], split + next + " ", cached)):
                    return True
                else:
                    i += 1
            else:
                cached.add(str)
                i += 1
        return False


def driver():
    dict_file = set(x.strip() for x in open('diction10k.txt'))

    for l in range(int(sys.stdin.readline().strip())):
        str = sys.stdin.readline().strip()
        print("phase number: {}".format(l+1))
        print("{}\n".format(str))

        print("iterative attempt:")
        iter_bool = iterCheck(dict_file,str,"")
        if (not iter_bool):
            print("NO, cannot be split\n")

        print("memoized attempt:")
        cached = set()
        mem_bool = memCheck(dict_file,str, "", cached)
        if (not mem_bool):
            print("NO, cannot be split\n")

if __name__ == '__main__':
    driver()
